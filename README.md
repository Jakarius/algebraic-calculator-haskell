# Haskell Project 1: Calculator #
## CS2006 ##
Due date: Tuesday, Week 4

### **Overview** ###

The objective of this project is to implement an interactive calculator application, supporting variables,
expression evaluation, error handling and command history. 

By completing this project, you should
become comfortable with the fundamental features and structure of a Haskell application, in particular:

* Programming with recursive equations
* Working with algebraic data types
* Manipulating lists
* Working with functional parsers

### **Preparation** ###

Some code is given as a starting point. You can download the code from the
Practicals/Haskell/Project1/Code directory on Studres. 

There are four files:
* Expr.hs, containing the definition of a basic expression type, an incomplete parser and an incomplete evaluator for expressions
* REPL.hs, containing the main loop (a Read-Eval-Print Loop) of the calculator application, and some incomplete helper functions
* Main.hs, which initialises the REPL
* Parsing.hs, which contains the parsing library discussed in lectures

You will only need to change Expr.hs and REPL.hs. You are free to edit the other files (but please
make sure you say which in your report if so.)

To run the calculator, you can either:
* Load Main.hs into ghci and run the function main. You can also use ghci to test individual
functions (and indeed I strongly recommend you do so!); 

or
* Compile to an executable with:
ghc --make Main.hs -o calc
at the terminal prompt, followed by running:
./calc

You will find that in its current form, most inputs produce the error:
*** Exception: Prelude.undefined

Review the lecture notes for Lecture 7 which give an overview of the architecture of interactive Haskell
applications like this one.

### **Basic Requirements** ###

The minimum requirement of this project is to implement the parts which are currently undefined
or applications of error, to complete the definition of eval so that addition works, and complete the
definition of process. This will give a calculator which supports addition only.

Further basic requirements will lead to a calculator which supports variable assignment and update,
all arithmetic operations, and clean error handling. In particular, you should include:

* A quit command, which exits the calculator
* As well as the provided Add: Operations for subtraction, multiplication and division, and extend the parser to support these
* Support for variable assignment using the Set command
* The evaluator State, which keeps track of:
1. The current values of variables
2. The number of calculations performed (this is the number shown in the prompt)
3. The command history
* A command for accessing command history, e.g. !3 would repeat the 3rd most recent command
* Parser improvements: in particular, it currently supports only single digit numbers! Also, it currently
does not support any whitespace.

* An implicit variable it which implicitly stores the result of the last operation
An example session with a basic working version supporting variables and command history might
look like the following:

0 > 4+5

9

1 > x=9

OK

1 > x=x+1

OK

1 > x

10

2 > !1

OK

2 > x

11

3 > 4+8

12

4 > it+4

16

4 > :q

Bye

### **General hints** ###

You may find it useful to test individual functions at the ghci prompt, rather than testing
the application as a whole. Consider using where for local function definitions where appropriate, and
using let to break down large functions into smaller steps.
Also, think carefully about the design before starting coding, and in particular think carefully about
how to divide the work between your team.

### **Additional Requirements** ###

NB: It is strongly recommended that you ensure you have completed the Basic Requirements
and have something to submit before you attempt these Additional Requirements!
In order to achieve a grade higher than 13, you should implement some of the following requirements;
for a grade higher than 17, you should implement all of the Medium requirements. You should
not feel limited by these; feel free to implement any feature you consider useful (but remember to describe
it in your report!)
* Easy: Extend the parser to support negative integers
* Easy: Support other functions, such as abs, mod or power
* Easy: Support floating point numbers rather than integers (and extend the parser accordingly)

* Medium: Support floating point numbers and integers. How would you deal with type mismatches
in the evaluator? Can you also support, for example, strings and string operations?
* Medium: Use a binary search tree for storing variables instead of a list of pairs
* Medium: Add a command to read input files containing a list of commands, rather than reading
input at the console
* Medium: Implement better treatment of errors: instead of using Maybe and using Nothing to
indicate errors, look at the Either type and consider how to use this to represent errors.

* Hard: Add commands for printing and looping. Can you extend this towards being a scripting
language?
* Hard: Look at the Haskell libraries and consider how to improve the input. In particular, take a
look at http://hackage.haskell.org/package/haskeline

* Very hard: What other commands would be useful? Consider for example a Simplify command
which simplifies an expression containing undefined variables. For example:
2 > :simplify 2*x+x
3*x
* Very hard: Allow defining functions as well as variables

### **Deliverables** ###

Hand in via MMS, by the deadline of 9pm on Tuesday of Week 4, a single .zip or .tar.gz file containing
two top level subdirectories called Code and Report, as follows:

* The Code directory should contain your group’s code, and should be the same for everyone in
the group. There may be further subdirectories if you wish, containing the source code in wellcommented
.hs files.
Everything that is needed to run your application should be in that directory or part of the Haskell
standard library — there should be no dependencies on external libraries. I should be able to run
your program from the command line of a bash shell.
* The Report directory should contain an individual report (of around 1500–2000 words), in PDF
format, describing your design and implementation and any difficulties you encountered. In
particular, it should include:
– A summary of the functionality of your program indicating the level of completeness with
respect to the Basic Requirements, and any Additional Requirements.
– Any known problems with your code, where you do not meet the Basic Requirements or any
Additional Requirements you attempted.

– Any specific problems you encountered which you were able to solve, and how you solved
them.
– An accurate summary of provenance, i.e. stating which files or code fragments were
1. written by you
2. modified by you from the source files provided for this assignment
3. sourced from elsewhere and who wrote them
– A description of your own contribution to the group work

### **Marking Guidelines** ###

This practical will be marked according to the guidelines at https://info.cs.st-andrews.ac.
uk/student-handbook/learning-teaching/feedback.html
To give an idea of how the guidelines will be applied to this project:
* The most simple solution which completes the definitions marked as undefined and error,
and which supports addition of integers, accompanied by a report, should get you a grade 7.
* Completing the basic requirements with a well-commented/documented, fully-working solution,
accompanied by a clear and informative report, should get you a grade 13. The report should
address all the Basic Requirements, and should make clear which of these you have completed.
* To achieve a grade higher than 17 you will need to implement the Easy and Medium additional
requirements listed below, with well-commented and documented code, accompanied by a clear
and informative report.
Finally, remember that:
* Standard lateness penalties apply as outlined in the student handbook at https://info.cs.
st-andrews.ac.uk/student-handbook/learning-teaching/assessment.html
* Guidelines for good academic practice are outlined in the student handbook at https://info.
cs.st-andrews.ac.uk/student-handbook/academic/gap.html

### **Finally** ###

Don’t forget to enjoy yourselves and use the opportunity to experiment and learn! If you have any
questions or problems please let me, a demonstrator, or your tutor know — don’t suffer in silence!
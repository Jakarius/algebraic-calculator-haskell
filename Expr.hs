module Expr where

import           Parsing

type Name = String

-- At first, 'Expr' contains only addition and values. You will need to
-- add other operations, and variables
data Expr = Add Expr Expr
          | Subtract Expr Expr
          | Multiply Expr Expr
          | Divide Expr Expr
          | Variable Name
          | Abs Expr
          | Val Int
  deriving Show

-- These are the REPL commands - set a variable name to a value, and evaluate
-- an expression
data Command = Set Name Expr
             | Eval Expr
  deriving Show

eval :: [(Name, Int)] -> -- Variable name to value mapping
        Expr -> -- Expression to evaluate
        Maybe Int -- Result (if no errors such as missing variables)
eval _ (Val x)        = Just x -- for values, just give the value directly
eval [] (Variable x) = Nothing
eval ((name,value):vars) (Variable x)   = if name == x then
                                            Just value
                                          else
                                            eval vars (Variable x)
eval vars (Add x y)      = do a <- eval vars x
                              b <- eval vars y
                              return (a + b)
eval vars (Subtract x y) = do a <- eval vars x
                              b <- eval vars y
                              return (a - b)
eval vars (Multiply x y) = do a <- eval vars x
                              b <- eval vars y
                              return (a * b)
eval vars (Divide x y)   = do a <- eval vars x
                              b <- eval vars y
                              case b of
                                0 -> Nothing
                                _ -> return (a `quot` b)
eval vars (Abs x)        = do a <- case eval vars x of
                                      Just b -> Just (absValue b)
                              return a

absValue :: (Ord a, Num a) => a -> a
absValue x
  | x >= 0    =  x
  | otherwise = -x

digitToInt :: Char -> Int
digitToInt x = fromEnum x - fromEnum '0'

pCommand :: Parser Command
pCommand = do t <- identifier
              character '='
              e <- pExpr
              return (Set t e)
              ||| do e <- pExpr
                     return (Eval e)

pExpr :: Parser Expr
pExpr = do t <- pTerm
           do character '+'
              e <- pExpr
              return (Add t e)
              ||| do character '-'
                     e <- pExpr
                     return (Subtract t e)
                     ||| return t

pTerm :: Parser Expr
pTerm = do f <- pFactor
           do character '*'
              t <- pTerm
              return (Multiply f t)
              ||| do character '/'
                     t <- pTerm
                     return (Divide f t)
                     ||| return f

pFactor :: Parser Expr
pFactor = do d <- integer
             return (Val d)
             ||| do v <- identifier
                    return (Variable v) -- This would be for referring to a var in a calculation.
                    ||| do character '('
                           e <- pExpr
                           character ')'
                           return e
                    ||| do character '|'
                           e <- pExpr
                           character '|'
                           return (Abs e)

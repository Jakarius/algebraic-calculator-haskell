module Main where

import           Expr
import           Parsing
import           REPL

main :: IO ()
main = repl initState

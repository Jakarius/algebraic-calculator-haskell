module REPL where

import           Expr
import           Parsing

data State = State { vars     :: [(Name, Int)],
                     numCalcs :: Int,
                     history  :: [Command] }
            deriving Show

initState :: State
initState = State [] 0 []

-- Given a variable name and a value, return a new set of variables with
-- that name and value added.
-- If it already exists, remove the old value
updateVars :: Name -> Int -> [(Name, Int)] -> [(Name, Int)]
updateVars n value ((name,v):vars)
  | n == name = (dropVar name ((name,v):vars)) ++ [(n, value)]
  | otherwise = (name,v):updateVars n value vars
updateVars n value [] = [(n, value)]

-- Return a new set of variables with the given name removed
dropVar :: Name -> [(Name, Int)] -> [(Name, Int)]
dropVar _ [] = []
dropVar n vars = [(name,value) | (name,value) <- vars, name /= n]

-- Add a command to the command history in the state
addHistory :: State -> Command -> State
addHistory st cmd = st { numCalcs = (numCalcs st) + 1, history = history st ++ [cmd] }

process :: State -> Command -> IO ()
process st (Set var e)
     = do let st' = case eval (vars st) e of
                      Just x -> st { vars = updateVars var x (vars st),
                                     history = history st ++ [(Set var e)] }
                       -- st' should include the variable set to the result of evaluating e
          repl st'
process st (Eval e)
     = do let st' = addHistory st (Eval e)
          -- Print the result of evaluation
          case eval (vars st') e of
            Just x            ->   putStrLn (show x)
            Nothing           ->   putStrLn "Undefined operation (dividing by zero? undefined variable?)."
          repl st'

-- Read, Eval, Print Loop
-- This reads and parses the input using the pCommand parser, and calls
-- 'process' to process the command.
-- 'process' will call 'repl' when done, so the system loops.

repl :: State -> IO ()
repl st = do putStr (show (numCalcs st) ++ {-show (history st) ++-} " > ")
             inp <- getLine
             case parse pCommand inp of
                  {-- parse pCommand returns a pair including a string of all the
                      input that wasn't consumed by the parser [(cmd, "stuff that wasn't parsed")]
                      If everything was parsed then the string in the pair will be empty
                      and the command will be valid and so sent to process. --}
                  [(cmd, "")] -> -- Must parse entire input
                          process st cmd
                  _ -> do putStrLn "Parse error"
                          repl st
